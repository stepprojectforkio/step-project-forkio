

const menuBtn = document.querySelector(".header__burger-menu")
const menu = document.querySelector(".header__menu")

function toggle() {
    menu.classList.toggle("header__menu--disable");
    menuBtn.classList.toggle("active")
}


menuBtn.addEventListener("click", toggle)

document.body.addEventListener('click', (e) =>{
    const target = e.target;
    const menuList = target === menu || menu.contains(target);
    const btnMenu = target === menuBtn;
    const menuClosed = menu.classList.contains('header__menu--disable');

    if (!menuList && !btnMenu && !menuClosed) {
        toggle();
    }
})
