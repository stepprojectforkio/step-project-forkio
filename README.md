# Step Project 2 Forkio


## Cклад учасників проєкту та визначення завдань:
1. Ольга Хомич
   * виконано завдання №2
2. Ольга Леус
   * виконано завдання №1



### У проєкті було використано наступні технології:
* JavaScript
* HTML
* preprocessor CSS (SASS)
* SVG
* BEM
* DOM
* NodeJs
* Інструменти контролю версій GitLab
* Webpack


### Посилання на сайт
https://stepprojectforkio.gitlab.io/step-project-forkio/



