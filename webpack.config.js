const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const miniCss = require("mini-css-extract-plugin");
const ImageMinimizerPlugin = require("image-minimizer-webpack-plugin");
const purgecss = require("@fullhuman/postcss-purgecss");
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
    context: path.resolve(__dirname, 'src'),
    mode: "production",
    entry:'./js/entry.js',
    output: {
        filename: "./js/script.min.js",
        path: path.resolve(__dirname, 'dist'),
        assetModuleFilename: "img/[name][ext]"
    },
    devtool: 'source-map',
    devServer: {
        static: {
            directory: path.join(__dirname, './'),
        },
        watchFiles: ['src/js/*.js', 'src/scss/**/*.scss'],
        historyApiFallback: true,
        open: true,
        compress: true,
        port: 8080,
        hot: true,
        devMiddleware:{
            writeToDisk: true,
        },
    },
    plugins: [
        new CleanWebpackPlugin(),
        new miniCss({
            filename: './css/style.min.css',
        }),
        new CopyPlugin({
            patterns: [
                { from: path.resolve(__dirname, 'src/img'), to: path.resolve(__dirname, 'dist/img')},
            ],
        }),
    ],
    module:{
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                }
            },
            {
                test: /\.scss$/,
                use: [
                    miniCss.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            url: true,
                        }
                    },
                    {
                        loader:'postcss-loader',
                        options: {
                            postcssOptions: {
                                plugins: [
                                    [
                                        "postcss-preset-env",
                                    ],
                                    [
                                        purgecss({
                                            content: ['./*.html']
                                        })
                                    ],
                                ],
                            },
                        },
                    },
                    'sass-loader']
            },
            {
                test: /\.(png|svg|jpg|jpeg|gif)$/i,
                type: 'asset/resource',

            }
        ]
    },
    optimization: {
        minimizer: [
            new ImageMinimizerPlugin({
                minimizer: {
                    implementation: ImageMinimizerPlugin.imageminMinify,
                    options: {
                        plugins: [
                            ["gifsicle", { interlaced: true }],
                            ["jpegtran", { progressive: true }],
                            ["optipng", { optimizationLevel: 5 }],
                            [
                                "svgo",
                                {
                                    plugins: [
                                        {
                                            name: "preset-default",
                                            params: {
                                                overrides: {
                                                    removeViewBox: false,
                                                    addAttributesToSVGElement: {
                                                        params: {
                                                            attributes: [
                                                                { xmlns: "http://www.w3.org/2000/svg" },
                                                            ],
                                                        },
                                                    },
                                                },
                                            },
                                        },
                                    ],
                                },
                            ],
                        ],
                    },
                },
            }),
            new CssMinimizerPlugin(),
            new TerserPlugin(),
        ],
        minimize: true,
    }
}